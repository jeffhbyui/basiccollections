import java.util.Date;

public class BlogPost {
    public String title;
    public String author;
    public Integer likes;
    public Date date;

    public BlogPost(String title, String author, Integer likes, Date date) {
        this.title = title;
        this.author = author;
        this.likes = likes;
        this.date = date;
    }

    public String toString() {
        return "Title: " + title + "Author: " + author + "Date: " + date + "\nLikes: " + likes;
    }
}
