import java.util.*;


public class CollectionTypes {

    static class LikesComparator implements Comparator<BlogPost> {

        // override the compare() method
        public int compare(BlogPost s1, BlogPost s2)
        {
            if (s1.likes == s2.likes)
                return 0;
            else if (s1.likes > s2.likes)
                return 1;
            else
                return -1;
        }
    }

    public static void main(String[] args) {

        System.out.println("------ LIST ------");
        List list = new ArrayList();
        list.add("Yo");
        list.add("listen");
        list.add("up");
        list.add("here's");
        list.add("a");
        list.add("story");
        list.add("about");
        list.add("a");
        list.add("little");
        list.add("guy");

        for (Object str : list) {
            System.out.println((String) str);
        }

        System.out.println("------ LIST ------");
        System.out.println("-- sorted --");
        for (Object str : list) {
            System.out.println((String) str);
        }

        System.out.println("------ SET ------");
        System.out.println("-- No duplicates, sorted --");
        Set set = new TreeSet();
        set.add("Yo");
        set.add("listen");
        set.add("up");
        set.add("here's");
        set.add("a");
        set.add("story");
        set.add("about");
        set.add("a");
        set.add("little");
        set.add("guy");

        for (Object str : set) {
            System.out.println((String) str);
        }

        System.out.println("----- QUEUE -----");
        System.out.println("-- uses natural ordering --");
        Queue queue = new PriorityQueue();
        queue.add("Yo");
        queue.add("listen");
        queue.add("up");
        queue.add("here's");
        queue.add("a");
        queue.add("story");
        queue.add("about");
        queue.add("a");
        queue.add("little");
        queue.add("guy");

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {
            System.out.println(queue.poll());
        }

        System.out.println("----- MAP -----");
        System.out.println("-- uses key val pair --");

        Map map = new HashMap();
        map.put(1, "Yo");
        map.put(2, "listen");
        map.put(3, "up");
        map.put(4, "here's");
        map.put(5, "a");
        map.put(6, "story");
        map.put(7, "about");
        map.put(8, "a");
        map.put(9, "little");
        map.put(10, "guy");

        for (int i = 1; i < map.size() + 1; i++) {
            String result = (String)map.get(i);
            System.out.println(result);
        }


        System.out.println("----- Generics List -----");
        List<BlogPost> blogPosts = new LinkedList<BlogPost>();
        blogPosts.add(new BlogPost( "How to make a blog. \n", "Mrs. Smart \n ", 20, new Date()));
        blogPosts.add(new BlogPost( "So your first blog didn't go well. \n", "Mrs. Smart \n ", 10, new Date()));
        blogPosts.add(new BlogPost( "Finding hobbies besides blogging. \n", "Mrs. Smart \n ", 5, new Date()));

        for (BlogPost blogposts : blogPosts ){
            System.out.println(blogposts);
            System.out.println(("\n"));
        }

        System.out.println("----- Generics List - blogPosts -  Sorted by Likes low to high -----");

        Collections.sort(blogPosts, new LikesComparator());
        System.out.println(blogPosts);
    }
}
